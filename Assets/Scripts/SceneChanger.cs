﻿using UnityEngine;
using System.Collections;

public class SceneChanger : MonoBehaviour {

	public string nextSceneName;
	float time = 0;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		// シーン移動直後はタップ判定をしない
		if (time < 0.5) {
			time += Time.deltaTime;
		} else if (Input.touchCount == 1 || Input.GetKeyDown( KeyCode.Space)) {
			Application.LoadLevel(nextSceneName);
		}
	}
}
